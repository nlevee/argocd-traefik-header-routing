# Argocd Traefik Header Routing

## Install Prereqs

```bash
helm upgrade --install --create-namespace --namespace=traefik \
    --repo https://helm.traefik.io/traefik \
    -f ./values/traefik.yaml traefik traefik
```

```bash
helm upgrade --install --create-namespace --namespace=argocd \
    --repo https://argoproj.github.io/argo-helm \
    -f ./values/argocd.yaml argocd argo-cd
```
